###########
# Imports #
###########

# Libraries
import pandas as pd

############
# Internal #
############


def export_file(df, file_name, orient='columns'):
    df.to_json(file_name, orient=orient)


def read_file(file_name):
    return pd.read_csv(file_name)
