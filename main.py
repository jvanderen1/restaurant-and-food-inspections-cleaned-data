###########
# Imports #
###########

# Utils
from file_management import *
from fix_dates import *
from fix_integers import *
from extract_lat_and_long import *

############
# Internal #
############


def main():
    # Read Files
    url = 'https://query.data.world/s/ytl7r6w6oroljx6loorqsvw2bpnztd'
    restaurant_inspection_df = read_file(url)

    # Extract Lat and Long
    extract_lat_and_long(restaurant_inspection_df, 'business_location')

    # Drop Unneeded Columns
    del restaurant_inspection_df['business_id']
    del restaurant_inspection_df['business_location']

    # Fix Dates
    fix_dates(restaurant_inspection_df, 'inspection_date')

    # Fix Integers
    fix_integers(restaurant_inspection_df, 'business_postal_code')
    fix_integers(restaurant_inspection_df, 'inspection_score')
    fix_integers(restaurant_inspection_df, 'violation_code')

    # Export File
    export_file(restaurant_inspection_df, 'cleaned.json')

    # Export each column
    columns = list(restaurant_inspection_df)

    for col in columns:
        export_file(restaurant_inspection_df[col].tail(10000), 'cleaned/{}.json'.format(col), 'values')

#########
# Entry #
#########


if __name__ == '__main__':
    main()
