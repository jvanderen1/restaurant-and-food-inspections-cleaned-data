###########
# Imports #
###########

# Libraries
import re

############
# Internal #
############


def extract_lat_and_long(df, column_name):
    exp = r'\((?P<latitude>.*),(?P<longitude>.*)\)'
    split_data = df[column_name].str.extract(exp)

    df["latitude"] = split_data['latitude'].astype(float)
    df["longitude"] = split_data['longitude'].astype(float)
