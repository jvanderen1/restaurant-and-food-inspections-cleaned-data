###########
# Imports #
###########

# Libraries
import numpy as np

############
# Internal #
############


def fix_integer(date):
    try:
        return int(date)
    except (ValueError, TypeError) as e:
        return -1


def fix_integers(df, column_name):
    df[column_name] = df[column_name].apply(fix_integer)
