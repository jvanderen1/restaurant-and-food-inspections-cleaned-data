###########
# Imports #
###########

# Libraries
from datetime import datetime
import pandas as pd

############
# Internal #
############

formats = (
    (r'%m/%d/%Y', r'%Y-%m-%d'),
)


def fix_date(date):
    for f in formats:
        try:
            date_obj = datetime.strptime(date, f[0])
            return date_obj.strftime(f[1])
        except (ValueError, TypeError) as e:
            pass

    # Default scenario is to return original data
    return date


def fix_dates(df, column_name):
    df[column_name] = df[column_name].apply(fix_date)
    df[column_name] = pd.to_datetime(df[column_name])
    df.sort_values(by=column_name, inplace=True, ascending=True)
